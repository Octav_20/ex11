package ro.orangetraining;

public class Generic <Employee> {
    private Employee epl;

    public Generic(Employee epl) {
        this.epl = epl;
    }

    public Employee get() {
        return epl;
    }


}
