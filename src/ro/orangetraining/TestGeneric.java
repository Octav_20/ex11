package ro.orangetraining;
import java.util.*;

public class TestGeneric {

    public static void main(String[] args) {
	// write your code here
        //String epl1,epl2;
        Generic<String> epl1=new Generic <> ("Samuel Jackson");
        Generic<String> epl2=new Generic <> ("Plaza hotel");
        System.out.println("My name is " + epl1.get());   
        System.out.println("I work as cashier at "+ epl2.get());
        Generic<Integer> sal= new Generic <> (2000);
        System.out.println("My salary is valued somewhere around " +sal.get()+" dollars");
    }
}
